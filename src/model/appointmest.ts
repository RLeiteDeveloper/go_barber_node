import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm'

@Entity('appointments')
class Appointmest {

  @PrimaryGeneratedColumn('uuid')
  id: String;

  @Column()
  provider: String;

  @Column('timestamp with time zone')
  date: Date;

}

export default Appointmest;
