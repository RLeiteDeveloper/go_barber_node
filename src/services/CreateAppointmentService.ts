import Appointmest from "../model/appointmest";
import { startOfHour } from 'date-fns';
import AppointmestRepository from '../repositories/appointmestRepository'
import { getCustomRepository } from 'typeorm';

interface CreateAppointment {
  provider: String,
  date: Date,
}

class CreateAppointmentService {

  public async execute({provider, date} : CreateAppointment) : Promise<Appointmest> {

    const appointmestRepository = getCustomRepository(AppointmestRepository);
    const parsedDate =  startOfHour(date);
    const findAppointmentInSameDate = await appointmestRepository.findByDate(parsedDate);

    if(findAppointmentInSameDate){
      throw Error('This appointment is already booked');
    }

    const appointment = appointmestRepository.create({
      provider,
      date: parsedDate
    });

    await appointmestRepository.save(appointment);

    return appointment;
  }

}

export default CreateAppointmentService;
