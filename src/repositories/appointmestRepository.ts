import Appointment from '../model/appointmest';
import { EntityRepository, Repository } from 'typeorm';


@EntityRepository(Appointment)
class AppointmestRepository extends Repository<Appointment> {

  public async findByDate(date: Date): Promise<Appointment | undefined> {
    const findAppointment = await this.findOne({
      where: {date},
    })
    return findAppointment;
  }

}

export default AppointmestRepository;
