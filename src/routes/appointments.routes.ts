import { Router, request, response } from 'express';
import { parseISO } from 'date-fns';
import AppointmestRepository from '../repositories/appointmestRepository'
import CreateAppointmentService from '../services/CreateAppointmentService';
import { getCustomRepository } from 'typeorm';

const appointmentsRouter = Router();

appointmentsRouter.get('/',  async (request, response) => {
  const appointmestRepository = getCustomRepository(AppointmestRepository);
  const appoitmensts = await appointmestRepository.find();
  return response.json(appoitmensts);
});

appointmentsRouter.post('/', async (request, response) => {
  try{

    const {provider, date} = request.body;
    const createAppointmentService = new CreateAppointmentService();
    const appointment = await createAppointmentService.execute({provider,date:parseISO(date)})
    return response.json(appointment);

  } catch (err) {

    return response.status(400).json({error: err.message});

  }

})

export default appointmentsRouter;
